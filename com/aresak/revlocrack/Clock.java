package com.aresak.revlocrack;

import com.aresak.revlocrack.botrp.BotRP;
import com.aresak.revlocrack.nopixel.Blacklist;
import com.aresak.revlocrack.nopixel.whois;
import com.aresak.revlocrack.utils.Debug;
import com.aresak.revlocrack.utils.HTTPConnection;
import org.jibble.pircbot.User;

import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ares7 on 27.09.2016.
 */
public class Clock implements Runnable {
    public int delay = 100;
    public Bot bot;

    public BotRP botRP;



    public Clock(Bot bot) {
        this.bot = bot;
    }


    Timer tickTimer = new Timer(true);
    TimerTask tickTask;

    public void run() {
        try {
            tickTask = new TimerTask() {
                @Override
                public void run() {
                    tick();
                }
            };

            tickTimer.scheduleAtFixedRate(tickTask, 0, delay);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
    }

    public long nextStreamerCheck = 0;
    private void tick() {
        if(nextStreamerCheck <= System.currentTimeMillis()) {

            HTTPConnection connection = new HTTPConnection("http://cryospark.symbiant.cz/nopixel.php");
            try {
                String[] streams = connection.sendAsGET().split(",");
                Map<String, String> characters = whois.characters();

                String newOnline = "";
                for(String stream : streams) {
                    if(bot.channels.containsKey("#" + stream))
                        continue;

                    if(Blacklist.isBlacklisted("#" + stream))
                        return;


                    if(characters.containsKey(stream)) {
                        newOnline += stream + ", ";
                    }
                    else {
                        bot.sendMessage(BotRP.twitchRP, "Channel " + stream + " just went online and is not listed yet!");
                        bot.sendMessage("#" + stream, "Hey there! I noticed you are online, my dumby owner will come for you soon to list you!");
                    }

                    bot.join("#" + stream);
                }

                if(newOnline.length() > 0)
                    bot.sendMessage(BotRP.twitchRP, "Channels " + newOnline + " just went online");
            } catch (Exception e) {
                e.printStackTrace();
            }

            nextStreamerCheck = System.currentTimeMillis() + (60 * 5 * 1000);
        }

        bot.channels.forEach((String ch, Channel channel) -> {
            // Each channel here


            // Refresh User List
            //channel.refreshUserList();
        });

        if(bot.goesShutdown) {
            if(System.currentTimeMillis() >= bot.shutdown) {
                try {
                    Thread.sleep(1000);
                    System.exit(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        if(botRP != null) {
            botRP.Tick();
        }
    }
}
