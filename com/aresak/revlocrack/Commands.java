package com.aresak.revlocrack;

import com.aresak.revlocrack.utils.InstaFile;

/**
 * Created by ares7 on 03.10.2016.
 */
public class Commands {
    public Channel parent;
    public Commands(Channel ch) {
        parent = ch;
    }


    public void addCommand(String command, String result) {
        InstaFile.write(InstaFile.escaped("C:/gameData/RevloCrack/data/" + parent.name + "/commands/" + command + ".txt"), result + ">>10>true");
    }

    public void removeCommand(String command) {
        InstaFile.delete(InstaFile.escaped("C:/gameData/RevloCrack/data/" + parent.name + "/commands/" + command + ".txt"));
    }

    public void setCooldown(String command, int seconds) {

    }

    public void disableCommand(String command) {

    }

    public boolean canUseCommand(String command) {
        return false;
    }
}
