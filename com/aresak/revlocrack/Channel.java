package com.aresak.revlocrack;

import com.aresak.revlocrack.utils.Debug;
import com.aresak.revlocrack.utils.InstaFile;
import org.jibble.pircbot.User;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by ares7 on 28.09.2016.
 */
public class Channel {
    public String name;
    public Bot bot;


    public ChatLog chatLog;
    public Map<String, TwitchUser> users = new HashMap<>();


    public boolean isMod = false;


    LinkedList<String> disabledCommands = new LinkedList<>();
    public int followageCooldown = 30;
    public long nextFollowAge = 0;


    public boolean connectedForList = false;


    public Channel(String name, Bot bot) throws Exception {
        this.name = name;
        this.bot = bot;

        chatLog = new ChatLog(this);

        InstaFile.write(InstaFile.escaped("C:/gameData/RevloCrack/data/" + name + "/mods.txt"), "");
        InstaFile.write(InstaFile.escaped("C:/gameData/RevloCrack/data/" + name + "/commands.disabled.txt"), "");
        InstaFile.write(InstaFile.escaped("C:/gameData/RevloCrack/data/" + name + "/op.txt"), "");


        isMod = Boolean.parseBoolean(InstaFile.read(InstaFile.escaped("C:/gameData/RevloCrack/data/" + name + "/op.txt")));

        reloadCommands();
    }


    public void checkUser(String user) {
        if(!users.containsKey(user))
            joined(user);
    }


    public boolean isMod(String user) {
        if(user.equals(InstaFile.escaped(name)) || user.equals("aresak"))
            return true;

        try {
            FileReader reader = new FileReader(InstaFile.escaped("C:/gameData/RevloCrack/data/" + name + "/mods.txt"));
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if(line.equals(""))
                    continue;

                if(line.equals(user))
                    return true;
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void mod(String user) {
        InstaFile.write(InstaFile.escaped("C:/gameData/RevloCrack/data/" + name + "/mods.txt"), user + "\r\n");
        bot.sendMessage(name, "Modded " + name + " to take care of BotRP <3");
        Debug.Log(user + " moded for " + name);
    }



    public void joined(String user) {
        TwitchUser t = new TwitchUser(user, this);
        users.put(user.toLowerCase(), t);
        Debug.Log("User " + user + " joined " + name);
        chatLog.UserLine(System.currentTimeMillis(), user.toLowerCase(), "{JOINED_CHANNEL}");

        if(bot.clock.botRP != null) {
            if(bot.clock.botRP.players.containsKey(user)) {
                bot.clock.botRP.players.get(user).user = t;
            }
            else {
                bot.clock.botRP.newPlayer(t);
            }
        }
    }

    public void left(String user) {
        users.remove(user.toLowerCase());
        Debug.Log("User " + user + " left " + name);
        chatLog.UserLine(System.currentTimeMillis(), user.toLowerCase(), "{LEFT_CHANNEL}");
    }

    public void reloadCommands() {
        disabledCommands.clear();

        try {
            FileReader reader = new FileReader(InstaFile.escaped("C:/gameData/RevloCrack/data/" + name + "/commands.disabled.txt"));
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if(line.equals(""))
                    continue;

                disabledCommands.add(line);
                Debug.Log("Disabled command " + line + " for " + name);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isDisabled(String command) {
        for(String cmd : disabledCommands) {
            if(cmd.toLowerCase().equals(command.toLowerCase()))
                return true;
        }

        return false;
    }

    public boolean isCooldown(String command) {
        try {
            FileReader reader = new FileReader(InstaFile.escaped("C:/gameData/RevloCrack/data/" + name + "/commands.txt"));
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if(line.equals(""))
                    continue;

                if(line.split(">>")[0].equals(command)) {
                    if(System.currentTimeMillis() < Integer.parseInt(line.split(">>")[1])) {
                        return false;
                    }
                    else {
                        return true;
                    }
                }
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean canSend(String command) {
        if(!isDisabled(command)) {
            if(!isCooldown(command)) {
                return true;
            }
        }
        return false;
    }

    public int getChatLines() {
        return getChatLines(name);
    }

    public static int getChatLines(String name) {
        int r = 0;
        try {
            FileReader reader = new FileReader(InstaFile.escaped("C:/gameData/RevloCrack/data/" + name + "/chat.txt"));
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                r ++;
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        Debug.Log(name + " has " + r + " lines");
        return r;
    }
}
