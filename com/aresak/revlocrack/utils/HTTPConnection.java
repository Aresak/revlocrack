package com.aresak.revlocrack.utils;


import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by ares7 on 22.06.2016.
 */
public class HTTPConnection {
    protected final String USER_AGENT = "Mozzila/5.0";

    protected String url;
    protected String params;
    protected HttpURLConnection con;

    public HTTPConnection(String _url) {
        url = _url;
    }

    public HTTPConnection(String _url, String _params) {
        url = _url;
        params = _params;
    }

    public String sendAsGET() throws Exception {
        URL obj = new URL(url + "?" + params);
        con = (HttpURLConnection) obj.openConnection();

        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", USER_AGENT);

        Debug.Log("[HTTPConnection] Sending GET request to " + url, 3);
        Debug.Log("[HTTPConnection] Parameters: " + params);
        Debug.Log("[HTTPConnection] Response Code: " + con.getResponseCode(), 3);

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream())
        );

        String inputLine;
        StringBuffer response = new StringBuffer();

        while((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();

        return response.toString();
    }

    public String sendAsPOST() throws Exception {
        URL obj = new URL(url);
        con = (HttpsURLConnection) obj.openConnection();

        con.setRequestMethod("POST");
        con.setRequestProperty("User-Agent", USER_AGENT);
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");

        con.setDoOutput(true);
        DataOutputStream wr = new DataOutputStream(con.getOutputStream());
        wr.writeBytes(params);
        wr.flush();
        wr.close();

        Debug.Log("[HTTPConnection] Sending POST request to " + url, 3);
        Debug.Log("[HTTPConnection] Parameters: " + params, 3);
        Debug.Log("[HTTPConnection] Response Code: " + con.getResponseCode());

        BufferedReader in = new BufferedReader(
                new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuffer response = new StringBuffer();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        in.close();

        return response.toString();
    }
}