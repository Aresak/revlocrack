package com.aresak.revlocrack.utils;

import com.sun.istack.internal.Nullable;

import java.time.LocalDateTime;
import java.util.Formatter;
import java.util.Locale;

/**
 * Created by ares7 on 22.06.2016.
 */
public class Debug {
    public static int debug_level = 0;
    // 0 - Errors Only
    // 1 - Errors and Warnings Only

    private static Formatter f;

    public static void Log() {
        System.out.println();
    }

    public static void LogWarning(String x, int level) {
        if(level > debug_level) System.out.format("[%s] Log Warning: %s\n", LocalDateTime.now ( ).toString ().replace ( "T", " " ), x);
    }

    public static void LogError(String x) {
        System.err.format("[%s] Log Error: %s\n", LocalDateTime.now ( ).toString ().replace ( "T", " " ), x);
    }

    public static void Log(String x, int level) {
        if(level > debug_level) System.out.format("[%s] Log: %s\n", LocalDateTime.now ( ).toString ().replace ( "T", " " ), x);
    }

    public static void Format(String x, @Nullable Object ... args) {
        if(f == null || (f.locale() != Locale.getDefault())) {
            f = new Formatter();
        }
        f.format(Locale.getDefault(), x, args);
        Debug.Log(f.toString());
        f = null;
    }

    public static void Log(String[][] x) {
        for(int a = 0; a < x.length; a ++) {
            Debug.Format("Array [%s] of size %s", a, x[a].length);
            for(int b = 0; b < x[a].length; b ++) {
                Debug.Format("> [%s][%s] %s", a, b, x[a][b]);
            }
        }
    }


    public static void Log(String x) {
        Log(x, 0);
    }
    public static void Log(int x) {
        Log(Integer.toString(x), 0);
    }
    public static void Log(long x) {
        Log(Long.toString(x), 0);
    }
    public static void Log(boolean x) {
        Log(Boolean.toString(x), 0);
    }
    public static void Log(char x) {
        Log(Character.toString(x), 0);
    }
    public static void Log(double x) {
        Log(Double.toString(x), 0);
    }
    public static void Log(float x) {
        Log(Float.toString(x), 0);
    }
    public static void Log(Object x) {
        Log(x.toString(), 0);
    }

    public static void Log(int x, int level) {
        Log(Integer.toString(x), level);
    }
    public static void Log(long x, int level) {
        Log(Long.toString(x), level);
    }
    public static void Log(boolean x, int level) {
        Log(Boolean.toString(x), level);
    }
    public static void Log(char x, int level) {
        Log(Character.toString(x), level);
    }
    public static void Log(double x, int level) {
        Log(Double.toString(x), level);
    }
    public static void Log(float x, int level) {
        Log(Float.toString(x), level);
    }
    public static void Log(Object x, int level) {
        Log(x.toString(), level);
    }


    public static void LogWarning(String x) {
        LogWarning(x, 0);
    }
    public static void LogWarning(int x) {
        LogWarning(Integer.toString(x), 0);
    }
    public static void LogWarning(long x) {
        LogWarning(Long.toString(x), 0);
    }
    public static void LogWarning(boolean x) {
        LogWarning(Boolean.toString(x), 0);
    }
    public static void LogWarning(char x) {
        LogWarning(Character.toString(x), 0);
    }
    public static void LogWarning(double x) {
        LogWarning(Double.toString(x), 0);
    }
    public static void LogWarning(float x) {
        LogWarning(Float.toString(x), 0);
    }
    public static void LogWarning(Object x) {
        LogWarning(x.toString(), 0);
    }

    public static void LogWarning(int x, int level) {
        LogWarning(Integer.toString(x), level);
    }
    public static void LogWarning(long x, int level) {
        LogWarning(Long.toString(x), level);
    }
    public static void LogWarning(boolean x, int level) {
        LogWarning(Boolean.toString(x), level);
    }
    public static void LogWarning(char x, int level) {
        LogWarning(Character.toString(x), level);
    }
    public static void LogWarning(double x, int level) {
        LogWarning(Double.toString(x), level);
    }
    public static void LogWarning(float x, int level) {
        LogWarning(Float.toString(x), level);
    }
    public static void LogWarning(Object x, int level) {
        LogWarning(x.toString(), level);
    }


    public static void LogError(int x) {
        LogError(Integer.toString(x));
    }
    public static void LogError(long x) {
        LogError(Long.toString(x));
    }
    public static void LogError(boolean x) {
        LogError(Boolean.toString(x));
    }
    public static void LogError(char x) {
        LogError(Character.toString(x));
    }
    public static void LogError(double x) {
        LogError(Double.toString(x));
    }
    public static void LogError(float x) {
        LogError(Float.toString(x));
    }
    public static void LogError(Object x) {
        LogError(x.toString());
    }

    public static boolean isArray(final Object obj) {
        return obj instanceof Object[] || obj instanceof boolean[] ||
                obj instanceof byte[] || obj instanceof short[] ||
                obj instanceof char[] || obj instanceof int[] ||
                obj instanceof long[] || obj instanceof float[] ||
                obj instanceof double[];
    }
}
