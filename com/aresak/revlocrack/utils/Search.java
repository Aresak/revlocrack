package com.aresak.revlocrack.utils;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by ares7 on 29.09.2016.
 */
public class Search {
    public static String bestResult(String[] options, String entry) {

        LinkedList<String> matches = new LinkedList<>();
        String[] entryA = entry.split(" ");

        for(String option : options) {
            if(option == null)
                continue;

            Debug.Log("Option " + option);
            String[] optionA = option.split(" ");
            for(String optionB : optionA) {
                for(String entryB : entryA) {
                    if(optionB.toLowerCase().equals(entryB.toLowerCase()))
                        matches.add(option);
                }
            }
        }


        String topMatch = "";
        int topMatchCount = 0;

        Map<String, Integer> matchesA = new HashMap<>();
        for(String match : matches) {
            if(matchesA.containsKey(match)) {
                int r = matchesA.get(match) + 1;
                matchesA.replace(match, r);
            }
            else {
                matchesA.put(match, 1);
            }
        }


        for(String option : options) {
            if(matchesA.containsKey(option)) {
                if(topMatchCount < matchesA.get(option)) {
                    topMatchCount = matchesA.get(option);
                    topMatch = option;
                }
            }
        }


        return topMatch;
    }
}
