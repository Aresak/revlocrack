package com.aresak.revlocrack.botrp;

import com.aresak.revlocrack.Bot;
import com.aresak.revlocrack.Channel;
import com.aresak.revlocrack.TwitchUser;
import com.aresak.revlocrack.utils.Debug;
import com.aresak.revlocrack.utils.InstaFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ares7 on 28.09.2016.
 */
public class BotRP {
    public static String twitchRP = "#_botrp_1475085199093";


    public Bot bot;
    public Channel channel;

    public RPCmdL listener;

    public Map<String, Player> players = new HashMap<>();

    public BotRP(Bot bot, Channel channel) {
        this.bot = bot;
        this.channel = channel;
        listener = new RPCmdL(this);

        syncPlayerList();

    }

    public void Tick() {

    }

    public Player getPlayer(String name) {
        return players.get(name.toLowerCase());
    }

    public void newPlayer(TwitchUser user) {

        if(false) {
            Player p = new Player();
            p.user = user;

            if(p.user.name.equals(bot.getName()))
                p.admin = true;

            InstaFile.write("C:/gameData/RevloCrack/data/" + channel.name + "/BotRP/players.txt", user.name + "\r\n");
            p.save();
            Debug.Log("[BotRP] New player created.");
            if(channel.name.equals(twitchRP)) {
                bot.sendMessage(channel.name, "A new account for " + user.name + " created. Welcome to Twitch RP!");
            }
        }
    }

    private void syncPlayerList() {
        InstaFile.write("C:/gameData/RevloCrack/data/" + channel.name + "/BotRP/players.txt", ""); // Init test

        players.clear();

        try {
            FileReader reader = new FileReader(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel.name + "/BotRP/players.txt"));
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if(line.equals(""))
                    continue;

                Player p = new Player();
                p.load(line, channel);
                //players.put(p.user.name, p);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void syncPlayer(TwitchUser user) {

        if(getPlayer(user.name) == null) {
            newPlayer(user);
        }
        else {
            players.get(user.name).user = user;
        }

    }
}
