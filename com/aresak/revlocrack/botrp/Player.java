package com.aresak.revlocrack.botrp;

import com.aresak.revlocrack.Channel;
import com.aresak.revlocrack.TwitchUser;
import com.aresak.revlocrack.utils.InstaFile;
import com.aresak.revlocrack.utils.SmartBlock;

/**
 * Created by ares7 on 28.09.2016.
 */
public class Player {
    public TwitchUser user;
    public Rank rank;


    public boolean admin = false;


    public long deadUntil = 0;


    public int cash = 0;


    public float health = 20;
    public float damage = 3;
    public float authority = 1;

    public float basicMultiplier = 0.158f;
    public int pointsDivider = 1000;


    public Player() {
        rank = new Rank();
        rank.faction = "Civilian";
        rank.rank_id = 0;
        rank.rank_name = "Citizen";
    }



    public void train() {
        float rv = 1;
        if(user.revloBotCurrency != 0)
            rv = (user.revloBotCurrency / pointsDivider);


        health += basicMultiplier * rv;
        damage += basicMultiplier * rv;
        authority += basicMultiplier * rv;
    }

    public void save() {
        SmartBlock sb = new SmartBlock("Player");
        sb.addProperty("name", user.name);
        sb.addProperty("op", user.isOP);

        sb.addProperty("faction", rank.faction);
        sb.addProperty("rank_name", rank.rank_name);
        sb.addProperty("rank_id", rank.rank_id);

        sb.addProperty("cash", cash);
        //sb.addProperty("euris", user.revloBotCurrency);
        sb.addProperty("admin", admin);
        sb.addProperty("dead", deadUntil);

        sb.addProperty("health", health);
        sb.addProperty("damage", damage);
        sb.addProperty("authority", authority);

        sb.addProperty("basic_multiplier", basicMultiplier);
        sb.addProperty("points_divider", pointsDivider);

        InstaFile.rewrite("C:/gameData/RevloCrack/data/" + user.channel.name + "/BotRP/players/" + user.name + ".txt", sb.process());
    }

    public void load(String name, Channel channel) {
        SmartBlock sb = new SmartBlock("Player");
        sb.process(InstaFile.read("C:/gameData/RevloCrack/data/" + channel.name + "/BotRP/players/" + name + ".txt"));

        rank.faction = sb.getProperty("faction");
        rank.rank_name = sb.getProperty("rank_name");
        rank.rank_id = sb.getIntProperty("rank_id");

        cash = sb.getIntProperty("cash");
        //user.revloBotCurrency = sb.getIntProperty("euris");
        admin = sb.getBoolProperty("admin");
        deadUntil = sb.getIntProperty("dead");

        health = sb.getFloatProperty("health");
        damage = sb.getFloatProperty("damage");
        authority = sb.getFloatProperty("authority");

        basicMultiplier = sb.getFloatProperty("basic_multiplier");
        pointsDivider = sb.getIntProperty("points_divider");
    }
}
