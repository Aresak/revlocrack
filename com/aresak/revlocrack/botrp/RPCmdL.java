package com.aresak.revlocrack.botrp;

import com.aresak.revlocrack.utils.Debug;
import com.aresak.revlocrack.utils.InstaFile;

/**
 * Created by ares7 on 28.09.2016.
 */
public class RPCmdL {
    public BotRP botRP;

    public RPCmdL(BotRP p) {
        botRP = p;
    }

    public void onMessage(String channel,
                          String sender,
                          String login,
                          String hostname,
                          String message) {
        Player player = botRP.getPlayer(sender);
        if(player == null)
            return;


        String[] bm = message.split(" ");
        if(player.admin) {
            // Admin commands
            switch (bm[0]) {
                case "!trp":
                    if(bm[1].equals("admin")) {
                        // Admin someone
                        Player p = botRP.getPlayer(bm[2]);
                        p.admin = true;
                        p.save();

                        InstaFile.write("C:/gameData/RevloCrack/data/" + channel + "/BotRP/admins.txt", p.user.name + "\r\n");


                        Debug.Log("[TwitchRP] Player " + p.user.name + " has been added to admin list");
                        if(channel.equals(botRP.twitchRP)) {
                            botRP.bot.sendMessage(channel, "Player " + p.user.name + " has been added to admin list");
                        }
                    }
                    break;
            }
        }


        // Regular Commands

    }
}
