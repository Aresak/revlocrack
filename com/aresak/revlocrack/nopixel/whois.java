package com.aresak.revlocrack.nopixel;

import com.aresak.revlocrack.Bot;
import com.aresak.revlocrack.utils.Debug;
import com.aresak.revlocrack.utils.HTTPConnection;
import com.aresak.revlocrack.utils.InstaFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by ares7 on 28.09.2016.
 */
public class whois {
    public static Map<String, String> characters() {
        Map<String, String> characters = new HashMap<>();


        InstaFile.write("C:/gameData/RevloCrack/data/nopixel.streamers.txt", "");
        try {
            FileReader reader = new FileReader(InstaFile.escaped("C:/gameData/RevloCrack/data/nopixel.streamers.txt"));
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if(line.equals(""))
                    continue;

                characters.put(line.split(":")[0], line.split(":")[1]);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }


        return characters;
    }

    public static void GetList(Bot bot) {
        HTTPConnection connection = new HTTPConnection("http://cryospark.symbiant.cz/nopixel.php");
        try {
            String[] streams = connection.sendAsGET().split(",");

            for(String stream : streams) {
                if(characters().containsKey(stream))
                    continue;

                bot.join("#" + stream);
                bot.channels.get("#" + stream).connectedForList = true;
                Debug.Log("Joining " + stream + " for listing");
                bot.sendMessage("#" + stream, "Hey there! I've found you listed on Twitch by keyword 'nopixel', if you want to be added to the list of Nopixel Streamers for viewers to fast search your twitch in chat, type in chat !iam {YOUR_RP_NAME_IN_NOPIXEL}. Only channel owner can set this. Cya MrDestructoid");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void ConnectToList(Bot bot) {
        characters().forEach((String twitch, String rp) -> {
            try {
                bot.join("#" + twitch);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public static void addCharacter(String rolePlayName, String twitchName) {
        InstaFile.write("C:/gameData/RevloCrack/data/nopixel.streamers.txt", twitchName + ":" + rolePlayName + "\r\n");
        Debug.Log("Added new streamer to the list");
    }

    static int whoisi = 0;
    public static String whois(String name, boolean op) {
        String[] names = name.split(" ");
        LinkedList<String> matches = new LinkedList<>();

        String[][] alll = new String[characters().size() + 1][2];

        boolean[] skipNext = {false};
        String[] skip = new String[2];


        characters().forEach((String twitch, String nme) -> {
            alll[whoisi][0] = nme;
            alll[whoisi][1] = twitch;
            whoisi ++;

            String[] snme = nme.split(" ");
            if(!skipNext[0]) {
                if(twitch.toLowerCase().equals(name.toLowerCase())) {
                    skip[1] = twitch;
                    skip[2] = nme;
                    skipNext[0] = true;
                }
                else {
                    if(nme.toLowerCase().equals(name)) {
                        matches.add(name);
                    }
                    else {
                        for(String nma : names) {
                            for(String nms : snme) {
                                if(nma.toLowerCase().equals(nms.toLowerCase())) {
                                    matches.add(nme);
                                }
                            }
                        }
                    }
                }
            }
        });
        whoisi = 0;


        String c = "";
        if(op)
            c = "http://twitch.tv/";

        if(skipNext[0])
            return "Streamer " + c + skip[1] + " is acting as " + skipNext[0] + " " + " at NoPixel";


        Map<String, Integer> count = new HashMap<>();
        for(String match : matches) {
            if(count.containsKey(match))
                count.replace(match, count.get(match) + 1);
            else
                count.put(match, 1);
        }

        String highestMatch = "";
        int highestMatchCount = 0;
        if(matches.size() > 0) {
            for(String match : matches) {
                if(count.get(match) > highestMatchCount) {
                    highestMatchCount = count.get(match);
                    highestMatch = match;
                }
            }
        }


        for(String[] all : alll) {
            if(all[0].toLowerCase().equals(highestMatch.toLowerCase())) {
                String easterEgg = "";
                switch (all[1].toLowerCase()) {
                    case "larryx7":
                        //easterEgg = "TriHard";
                        break;
                }

                return "Streamer " + c + all[1] + " is acting as " + all[0] + " " + easterEgg + " at NoPixel";
            }
        }

        return "Didn't found any matched streamer x(";
    }
}
