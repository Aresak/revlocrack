package com.aresak.revlocrack.nopixel;

import com.aresak.revlocrack.utils.InstaFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ares7 on 30.09.2016.
 */
public class Wiki {
    public static Map<String, Wiki> wiki = new HashMap<>();

    public String name = "";
    public String content = "";

    public Wiki(String name, String content) {
        this.name = name;
        this.content = content;
    }


    public static void wikiReload() {
        InstaFile.write("C:/gameData/RevloCrack/data/wiki.txt", "");
        try {
            FileReader reader = new FileReader(InstaFile.escaped("C:/gameData/RevloCrack/data/wiki.txt"));
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if(line.equals(""))
                    continue;

                Wiki tempWiki = new Wiki(line.split(">>")[0], line.split(">>")[1]);
                wiki.put(tempWiki.name, tempWiki);
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String[] wikiOptions() {
        wikiReload();

        String[] r = new String[wiki.size()];
        final int[] i = {0};
        wiki.forEach((String name, Wiki wiki) -> {
            r[i[0]] = name;
            i[0]++;
        });
        i[0] = 0;
        return r;
    }
}
