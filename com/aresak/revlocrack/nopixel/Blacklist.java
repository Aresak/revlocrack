package com.aresak.revlocrack.nopixel;

import com.aresak.revlocrack.Channel;
import com.aresak.revlocrack.utils.Debug;
import com.aresak.revlocrack.utils.InstaFile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by ares7 on 02.10.2016.
 */
public class Blacklist {
    public static boolean isBlacklisted(String channel) {
        InstaFile.write("C:/gameData/RevloCrack/data/blacklist.streamers.txt", "");
        try {
            FileReader reader = new FileReader(InstaFile.escaped("C:/gameData/RevloCrack/data/blacklist.streamers.txt"));
            BufferedReader bufferedReader = new BufferedReader(reader);
            String line;

            while ((line = bufferedReader.readLine()) != null) {
                if(line.equals(""))
                    continue;

                if(line.toLowerCase().equals(channel))
                    return true;
            }
            reader.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void blacklist(Channel channel, String reason) {
        InstaFile.write("C:/gameData/RevloCrack/data/blacklist.streamers.txt", channel.name + ">>" + reason);
        Debug.Log("Blacklisted channel " + channel.name + " - " + reason);
    }
}
