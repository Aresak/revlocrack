package com.aresak.revlocrack.nopixel;


/**
 * Created by ares7 on 29.09.2016.
 */
public class Ch {
    public String name;
    public int rows;


    public Ch compare(Ch ch1, Ch ch2) {
        if(ch1.rows < ch2.rows)
            return ch2;
        else
            return ch1;
    }

    public static Ch[] bubble_srt(Ch[] channels) {
        int n = channels.length;
        int k;

        for(int m = n; m >= 0; m --) {
            for(int i = 0; i < n - 1; i ++) {
                k = i + 1;
                if(channels[i].rows > channels[k].rows) {
                    Ch tch = channels[i];
                    channels[i] = channels[k];
                    channels[k] = tch;
                }
            }
        }

        return channels;
    }
}
