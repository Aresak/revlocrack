package com.aresak.revlocrack.nopixel;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by ares7 on 29.09.2016.
 */
public class Item {
    public int copper = 0;
    public int iron = 0;
    public int silver = 0;
    public int wood = 0;
    public int oil = 0;
    public int fur_pelt = 0;

    public String name = "";

    public Item(String name, int copper, int iron, int silver, int wood, int oil, int fur_pelt) {
        this.copper = copper;
        this.iron = iron;
        this.silver = silver;
        this.wood = wood;
        this.oil = oil;
        this.fur_pelt = fur_pelt;
        this.name = name;
    }


    public static Map<String, Item> generateItems() {
        Map<String, Item> map = new HashMap<>();

        // Pistols
        /*map.put("1911 ACP", new Item("1911 ACP", 1, 0, 0, 1, 0, 0));
        map.put("4-five .45 ACP", new Item("4-five .45 ACP", 0, 1, 0, 1, 0, 0));
        map.put("ACP-C2 .45 ACP", new Item("ACP-C2 .45 ACP", 1, 0, 0, 1, 0, 0));*/
        map.put("1911 ACP", new Item(
                "1911 ACP", // Name
                1, // Copper
                0, // Iron
                0, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("4-five .45 ACP", new Item(
                "4-five .45 ACP", // Name
                0, // Copper
                1, // Iron
                0, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("ACP-C2 .45 ACP", new Item(
                "ACP-C2 .45 ACP", // Name
                1, // Copper
                0, // Iron
                0, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("Browning HP", new Item(
                "Browning HP", // Name
                0, // Copper
                0, // Iron
                0, // Silver
                0, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("Colt 1911", new Item(
                "Colt 1911", // Name
                0, // Copper
                1, // Iron
                0, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("CZ 75 COMPACT", new Item(
                "CZ 75 COMPACT", // Name
                1, // Copper
                0, // Iron
                0, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("Luger", new Item(
                "Luger", // Name
                1, // Copper
                0, // Iron
                0, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("Makarov PM", new Item(
                "Makarov PM", // Name
                0, // Copper
                1, // Iron
                0, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("PB 6P9", new Item(
                "PB 6P9", // Name
                0, // Copper
                1, // Iron
                0, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("M9", new Item(
                "M9", // Name
                0, // Copper
                0, // Iron
                1, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("Taurus Tracker Model 455", new Item(
                "Taurus Tracker Model 455", // Name
                0, // Copper
                0, // Iron
                1, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts
        map.put("Tokarev", new Item(
                "Tokarev", // Name
                0, // Copper
                0, // Iron
                1, // Silver
                0, // Wood
                1, // Oil
                0)); // Fur Pelts
        map.put("USP40 Match", new Item(
                "USP40 Match", // Name
                0, // Copper
                0, // Iron
                1, // Silver
                0, // Wood
                1, // Oil
                0)); // Fur Pelts
        map.put("Kimber Desert Warrior", new Item(
                "Kimber Desert Warrior", // Name
                1, // Copper
                0, // Iron
                0, // Silver
                1, // Wood
                2, // Oil
                0)); // Fur Pelts
        map.put("FN Five-Seven", new Item(
                "FN Five-Seven", // Name
                1, // Copper
                0, // Iron
                0, // Silver
                1, // Wood
                3, // Oil
                0)); // Fur Pelts
        map.put("FNP45 Tactical", new Item(
                "FNP45 Tactical", // Name
                0, // Copper
                1, // Iron
                0, // Silver
                1, // Wood
                3, // Oil
                0)); // Fur Pelts
        map.put("Desert Eagle Gold", new Item(
                "Desert Eagle Gold", // Name
                0, // Copper
                1, // Iron
                0, // Silver
                1, // Wood
                7, // Oil
                0)); // Fur Pelts
        map.put("Desert Eagle Silver", new Item(
                "Desert Eagle Silver", // Name
                5, // Copper
                0, // Iron
                1, // Silver
                1, // Wood
                7, // Oil
                0)); // Fur Pelts
        map.put("Pistol Mag", new Item(
                "Pistol Mag", // Name
                1, // Copper
                0, // Iron
                0, // Silver
                1, // Wood
                0, // Oil
                0)); // Fur Pelts


        // Large Weapons
        map.put("Micro UZI PDW", new Item(
                "Micro UZI PDW", // Name
                3, // Copper
                0, // Iron
                2, // Silver
                2, // Wood
                2, // Oil
                0)); // Fur Pelts
        map.put("Sa-61", new Item(
                "Sa-61", // Name
                2, // Copper
                2, // Iron
                2, // Silver
                1, // Wood
                2, // Oil
                0)); // Fur Pelts
        map.put("Glock 18", new Item(
                "Clock 18", // Name
                3, // Copper
                0, // Iron
                3, // Silver
                2, // Wood
                4, // Oil
                0)); // Fur Pelts
        map.put("Intratec TEC-9", new Item(
                "Intratec TEC-9", // Name
                0, // Copper
                2, // Iron
                1, // Silver
                1, // Wood
                7, // Oil
                0)); // Fur Pelts
        map.put("Micro UZI", new Item(
                "Micro UZI", // Name
                2, // Copper
                0, // Iron
                1, // Silver
                0, // Wood
                8, // Oil
                0)); // Fur Pelts
        map.put("Large Mag", new Item(
                "Large Mag", // Name
                2, // Copper
                2, // Iron
                2, // Silver
                5, // Wood
                2, // Oil
                0)); // Fur Pelts

        // Items
        map.put("Portable Meth Lab", new Item("Portable Meth Lab", 1, 1, 1, 1, 10, 0));
        map.put("Weed Growing Plot", new Item("Weed Growing Plot", 1, 0, 0, 15, 0, 0));
        map.put("Underwater FishStick Dynamite", new Item("Underwater FishStick Dynamite", 4, 2, 2, 0, 5, 0));
        map.put("Live Dynamite", new Item("Live Dynamite", 3, 5, 5, 0, 5, 0));
        map.put("Hacked ID Card", new Item("Hacked ID Card", 1, 5, 5, 0, 5, 0));
        map.put("Lockpick", new Item("Lockpick", 5, 0, 0, 2, 0, 0));
        map.put("Grappling Hook", new Item("Grappling Hook", 7, 1, 1, 0, 1, 0));
        map.put("Heavy Duty Drill", new Item(
                "Heavy Duty Drill", // Name
                1, // Copper
                1, // Iron
                1, // Silver
                1, // Wood
                2, // Oil
                0)); // Fur Pelts
        map.put("Engine Repair Kit", new Item(
                "Engine Repair Kit", // Name
                5, // Copper
                0, // Iron
                1, // Silver
                0, // Wood
                2, // Oil
                0)); // Fur Pelts
        map.put("Car Wheel Repair Kit", new Item(
                "Car Wheel Repair Kit", // Name
                1, // Copper
                0, // Iron
                0, // Silver
                0, // Wood
                1, // Oil
                0)); // Fur Pelts
        map.put("Regular Clothing", new Item(
                "Regular Clothing", // Name
                0, // Copper
                0, // Iron
                0, // Silver
                0, // Wood
                0, // Oil
                2)); // Fur Pelts
        map.put("Quality Clothing", new Item(
                "Quality Clothing", // Name
                0, // Copper
                0, // Iron
                0, // Silver
                0, // Wood
                0, // Oil
                4)); // Fur Pelts


        return map;
    }


    public static String[] itemOptions() {
        Map<String, Item> map = generateItems();
        String[] s = new String[map.size()];

        final int[] i = {0};
        map.forEach((String a, Item b) -> {
            s[i[0]] = a;
            i[0]++;
        });

        return s;
    }
}
