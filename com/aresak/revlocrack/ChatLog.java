package com.aresak.revlocrack;

import com.aresak.revlocrack.utils.Debug;
import com.aresak.revlocrack.utils.InstaFile;

/**
 * Created by ares7 on 28.09.2016.
 */
public class ChatLog {
    public int lines = 0;
    public Channel channel;

    public ChatLog(Channel channel) {
        this.channel = channel;
    }

    public void UserLine(long time, String user, String message) {
        String line = time + " " + user + ": " + message;
        InstaFile.write("C:/gameData/RevloCrack/data/" + channel.name + "/chat.txt", line + "\r\n");
        Debug.Log("Writen new line to " + channel.name + " chat log: " + line);
        lines ++;
    }
}
