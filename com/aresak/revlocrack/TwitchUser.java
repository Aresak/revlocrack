package com.aresak.revlocrack;

import com.aresak.revlocrack.utils.Debug;


/**
 * Created by ares7 on 28.09.2016.
 */
public class TwitchUser {
    public String name;
    public boolean isOP;

    public Channel channel;

    // RevloBot Details
    public int revloBotCurrency = 0;
    public long canGambleAgainTime = 0;


    public TwitchUser(String name, Channel channel) {
        this.name = name;
        this.channel = channel;

        Debug.Log("New user " + name + " initiated");
    }

    public void setOP() {
        isOP = true;
    }

    public void justGambled() {
        canGambleAgainTime = System.currentTimeMillis() + (60 * 5 * 1000);
        Debug.Log("Updated gamble time for " + name);
    }

    public boolean canGamble() {
        return canGambleAgainTime <= System.currentTimeMillis();
    }
}
