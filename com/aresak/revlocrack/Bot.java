package com.aresak.revlocrack;

import com.aresak.revlocrack.botrp.BotRP;
import com.aresak.revlocrack.nopixel.*;
import com.aresak.revlocrack.utils.Debug;
import com.aresak.revlocrack.utils.HTTPConnection;
import com.aresak.revlocrack.utils.InstaFile;
import com.aresak.revlocrack.utils.Search;
import org.jibble.pircbot.PircBot;
import org.jibble.pircbot.User;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * Created by ares7 on 27.09.2016.
 */
public class Bot extends PircBot {
    public Clock clock;
    public String target;

    public boolean goesShutdown = false;
    public long shutdown = 0;

    public Map<String, Channel> channels = new HashMap<>();


    public Bot() {
        this.setName("botrp");
        this.isConnected();

        clock = new Clock(this);
        Thread t = new Thread(clock);
        t.run();
    }


    public void join(String channel) throws Exception {
        Debug.Log("Joined " + channel);
        this.joinChannel(channel);
        channels.put(channel, new Channel(channel, this));

        if(channel.equals("#euri") || channel.equals(BotRP.twitchRP)) {
            clock.botRP = new BotRP(this, channels.get(channel));
        }

        //sendMessage(channel, "MrDestructoid");
    }



    String lastGambler = "";
    // Channel Commands
    private void gambleTimeCommand(String sender, String channel) {
        TwitchUser tu = channels.get(channel).users.get(sender);
        int timet = (int) (tu.canGambleAgainTime - System.currentTimeMillis()) / 1000;

        if(timet <= 0) {
            Debug.Log(sender + " can already gamble!");
            return;
        }

        int minutes = timet / 60;
        int seconds = timet % 60;

        sendMessage(channel, sender + " can gamble in " + (minutes > 0 ? minutes + ":" + seconds : seconds + "s"));
    }



    // PircBot methods


    int jonCounterDay = 0;
    int jonArrDayCounter = 0;
    public void onMessage(String channel,
                          String sender,
                          String login,
                          String hostname,
                          String message) {
        long time = System.currentTimeMillis();
        String[] bm = message.split(" ");

        channels.get(channel).checkUser(sender);

        channels.get(channel).chatLog.UserLine(time, sender, message);

        if(clock.botRP != null) {
            clock.botRP.listener.onMessage(channel, sender, login, hostname, message);
        }

        for(String b : bm) {
            switch(b.toLowerCase()) {
                case "@botrp":
                case "botrp":
                    if(sender.equals("aresak"))
                        return;
                    Debug.Log(sender + " has mentioned you at " + channel + ": " + message);
                    sendMessage(BotRP.twitchRP, "/w aresak " + sender + " has mentioned you at " + channel + ": " + message);
                    break;
            }
        }


        // General Commands
        if(bm[0].toLowerCase().equals("!followage")) {
            if(channels.get(channel).isDisabled("!followage")) {
                Debug.LogWarning("Command " + message + " is disabled for " + channel);
                return;
            }

            if(channels.get(channel).nextFollowAge > System.currentTimeMillis()) {
                Debug.LogWarning("Command " + message + " has cooldown for " + channel);
                return;
            }
            channels.get(channel).nextFollowAge = System.currentTimeMillis() + (channels.get(channel).followageCooldown * 1000);

            String follower = sender;
            if(bm.length == 2) {
                follower = bm[1].toLowerCase();
            }

            HTTPConnection httpConnection = new HTTPConnection("http://twitch.kactus.xyz/api/followage.php", "channel=" + InstaFile.escaped(channel) + "&user=" + follower);
            try {
                String r = httpConnection.sendAsGET();
                if(r.equals("")) {
                    sendMessage(channel, (sender.equals(follower) ? "You are" : "@" + follower + " is") + " not following " + channel);
                    return;
                }

                sendMessage(channel, (sender.equals(follower) ? "You have" : "@" + follower + " has") + " been following " + channel + " for " + r);
                return;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        else if(bm[0].toLowerCase().equals("!whois")) {
            if(channels.get(channel).isDisabled("!whois"))
                return;

            String name = "";
            for(int i = 1; i < bm.length; i ++) {
                name += bm[i];
                if(i != bm.length - 1) name += " ";
            }
            sendMessage(channel, whois.whois(name.toLowerCase(), channels.get(channel).isMod));
        }
        else if(bm[0].toLowerCase().equals("!botrp")) {
            if(channels.get(channel).isDisabled("!botrp"))
                return;
            if(bm.length > 1) {
                switch(bm[1]) {
                    case "online":
                        if(!sender.equals("aresak"))
                            return;
                        final String[] p = {""};
                        channels.forEach((String twitch, Channel ch) -> {
                            p[0] += twitch + ", ";
                        });
                        sendMessage(channel, "BotRP is currently live on " + p[0]);
                        break;
                    case "search":
                        String p1 = "";
                        for(int i = 2; i < bm.length; i ++) {
                            p1 += bm[i] + " ";
                        }

                        String searched = Search.bestResult(Item.itemOptions(), p1);
                        if(!searched.equals("")) {
                            Debug.Log("Found crafting recipe for " + p1);
                            Item item = Item.generateItems().get(searched);
                            String res = "Recipe " + searched + " | Wood: " + item.wood
                                    + ", Copper: " + item.copper + ", Iron: " + item.iron + ", Silver: " + item.silver + ", Oil: " + item.oil + ", Pelts: " + item.fur_pelt;
                            sendMessage(channel, res);
                        }
                        else {
                            String s2 = Search.bestResult(Wiki.wikiOptions(), p1);

                            if(!s2.equals("")) {
                                Debug.Log("Found on wiki for " + p1);
                                Wiki w = Wiki.wiki.get(s2);
                                String res = "[WIKI] (" + w.name + ") " + w.content;
                                sendMessage(channel, res);
                            } else {
                                sendMessage(channel, "Didn't found any data for " + p1 + " NotLikeThis");
                            }
                        }

                        break;
                }
            }
            else
                sendMessage(channel, "Worried if the RP character on nopixel streams? Do \"!whois {name, or part}\" to find out! | To disable some commands do \"!cmd disable !command\" | Mod \"!mod {name}\"");
        }

        // General Moderation Commands
        switch(bm[0].toLowerCase()) {
            case "!cmd":
                switch(bm[1].toLowerCase()) {
                    case "disable":
                        if(channels.get(channel).isMod(sender)) {
                            InstaFile.write(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel + "/commands.disabled.txt"), bm[2].toLowerCase() + "\r\n");
                            sendMessage(channel, "Command " + bm[2] + " disabled for " + InstaFile.escaped(channel));
                            channels.get(channel).reloadCommands();
                        }
                        break;
                    case "enabled":

                        break;
                    case "reload":
                        if(channels.get(channel).isMod(sender)) {
                            channels.get(channel).reloadCommands();
                        }
                        break;
                    case "cooldown":
                        if(bm[2].toLowerCase().equals("!followage")) {
                            channels.get(channel).followageCooldown = Integer.parseInt(bm[3]);
                            sendMessage(channel, "Command " + bm[2] + " has been set to " + bm[3] + "s cooldown");

                            goesShutdown = true;
                            shutdown = System.currentTimeMillis() + (60 * 1000);
                        }
                        break;
                }
                break;
            case "!mod":
                if(channels.get(channel).isMod(sender)) {
                    channels.get(channel).mod(bm[1].toLowerCase());
                }
                break;
            case "!op":
                if(channels.get(channel).isMod) {
                    InstaFile.rewrite(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel + "/op.txt"), "false");
                    channels.get(channel).isMod = false;
                } else {
                    InstaFile.rewrite(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel + "/op.txt"), "true");
                    channels.get(channel).isMod = true;
                }
                break;
            case "!lines":
                if(bm.length == 1) {
                    sendMessage(channel, "This channel has " + channels.get(channel).getChatLines() + " lines of chat during nopixel streams!");
                    return;
                }

                switch(bm[1].toLowerCase()) {
                    case "all":
                        switch(bm[2].toLowerCase()) {
                            case "top":
                                int size = Integer.parseInt(bm[3]) + 1;

                                // Fetch
                                LinkedList<Ch> list = new LinkedList<>();
                                whois.characters().forEach((String twitch, String rpName) -> {
                                    Ch ch = new Ch();
                                    ch.name = twitch;
                                    ch.rows = Channel.getChatLines("#" + twitch);
                                    list.add(ch);
                                });

                                // Sort
                                Ch[] all = new Ch[list.size()];
                                for(int i = 0; i < list.size(); i ++) {
                                    all[i] = list.get(i);
                                }

                                all = Ch.bubble_srt(all);


                                int a = 1;
                                String res = "Nopixel streams with most chat lines: ";
                                for(int i = all.length - 1; i != all.length - size; i --) {
                                    res += a + ". " + all[i].name + ": " + all[i].rows;
                                    if(i != all.length + 1) res += ", ";
                                    a ++;
                                }
                                sendMessage(channel, res);
                                break;
                        }
                        break;
                }
                break;
        }

        // Check if is command disabled


        if(sender.equals("aresak")) {
            // General Aresak only commands
            if(bm[0].equals("!fuck")) {
                String p = "";
                for(int i = 1; i < bm.length; i ++) {
                    p += bm[i] + " ";
                }
                sendMessage(channel, p);
            }
            else if(bm[0].toLowerCase().equals("!nopixel")) {
                if(bm[1].toLowerCase().equals("streamer")) {
                    if(bm[2].toLowerCase().equals("add")) {
                        String p = "";
                        for(int i = 3; i < bm.length; i ++) {
                            p += bm[i] + " ";
                        }

                        whois.addCharacter(p, InstaFile.escaped(channel));
                        sendMessage(channel, "Added " + InstaFile.escaped(channel) + " as streamer of NoPixel character " + p);
                    }
                    else if(bm[2].toLowerCase().equals("push")) {
                        String p = "";
                        for(int i = 4; i < bm.length; i ++) {
                            p += bm[i] + " ";
                        }

                        whois.addCharacter(p, InstaFile.escaped(bm[3].toLowerCase()));
                        sendMessage(channel, "Added " + InstaFile.escaped(bm[3]) + " as streamer of NoPixel character " + p);
                    }
                }
                else if(bm[1].toLowerCase().equals("check")) {
                    whois.GetList(this);
                }
                else if(bm[1].toLowerCase().equals("connect")) {
                    whois.ConnectToList(this);
                }
            }
            else if(bm[0].toLowerCase().equals("!say")) {
                String p = "";
                for(int i = 2; i < bm.length; i ++) {
                    p += bm[i] + " ";
                }
                sendMessage("#" + bm[1], p);
            }
            else if(bm[0].toLowerCase().equals("!blacklist")) {
                String p = "";
                for(int i = 1; i < bm.length; i ++) {
                    p += bm[i] + " ";
                }
                Blacklist.blacklist(channels.get(channel), p);
                sendMessage(channel, "This channel has been blacklisted for the nopixel bot (" + p + ")");
            }
        }

        if(channels.get(channel).connectedForList) {
            if(sender.equals(InstaFile.escaped(channel))) {
                if(bm[0].toLowerCase().equals("!iam")) {
                    String p = "";
                    for(int i = 3; i < bm.length; i ++) {
                        p += bm[i] + " ";
                    }

                    whois.addCharacter(p, InstaFile.escaped(channel));
                    sendMessage(channel, "Added " + InstaFile.escaped(channel) + " as streamer of NoPixel character " + p);
                }
            }
        }


        if(channel.equals("#aresak") || channel.equals("#_aresak_1475083981411")) {
            // Bot master commands
            if(sender.equals("aresak")) {
                // Allowed
                switch (bm[0]) {
                    case "!join":
                        try {
                            join(bm[1]);
                            sendMessage(channel, "Bot joined " + bm[1]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case "!leave":
                        partChannel(bm[1]);
                        sendMessage(channel, "Bot left " + bm[1]);
                        break;
                }
            }
        }
        else if(channel.equals("#euri")) {
            // Euri only commands
            if(sender.equals("aresak")) {
                // Aresak only commands
                if(bm[0].equals("!run") && bm[1].equals("rp")) {
                    clock.botRP = new BotRP(this, channels.get(channel));
                    Debug.Log("Euri RP initialized");
                }
            }


            if(message.toLowerCase().equals("@aresak !time")) {

                gambleTimeCommand(sender, channel);

            }

            if(bm[0].toLowerCase().equals("!gamble")) {
                lastGambler = sender;
                Debug.Log("Set last gambler to " + sender);
            }

            if(sender.equals("revlobot")) {
                // RevloBot based responses
                if(bm[0].toLowerCase().equals("rolled")) {
                    // Someone rolled
                    String roller = bm[2].toLowerCase();
                    int currency = Integer.parseInt(bm[9]);

                    Debug.Log(roller + " gambled and syncing player currency (" + currency + ")");
                    TwitchUser tu =  channels.get(channel).users.get(roller);
                    tu.justGambled();
                    tu.revloBotCurrency = currency;
                }
                else if(bm[1].toLowerCase().equals("has") && bm[3].toLowerCase().equals("euris")) {
                    // Currency status
                    TwitchUser tu = channels.get(channel).users.get(bm[0].toLowerCase());
                    tu.revloBotCurrency = Integer.parseInt(bm[2]);
                    Debug.Log(tu.name + " currency synced (" + bm[2] + ")");
                }
                else if(message.toLowerCase().equals("you can only gamble once every 5.0 minutes.")) {

                    gambleTimeCommand(lastGambler, channel);

                }
            }
        }
        if(channel.equals("#executorjoel") || channel.equals("#euri")) {
            switch(bm[0]) {
                case "!death":
                    if(channels.get(channel).isMod(sender)) {
                        String n = Integer.toString(Integer.parseInt(InstaFile.read(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel + "/deathcount.txt"))) + 1);
                        InstaFile.rewrite(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel + "/deathcount.txt"), n);
                        jonCounterDay ++;
                        sendMessage(channel, "LUL He died " + n + "x LUL " + jonCounterDay + "x today 4Head");
                    }
                    break;
                case "!deaths":
                    String n = InstaFile.read(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel + "/deathcount.txt"));
                    sendMessage(channel, "Mr Priest died " + n + "x and just " + jonCounterDay + "x today 4Head");
                    break;
                case "!arrested":
                    if(channels.get(channel).isMod(sender)) {
                        String b = Integer.toString(Integer.parseInt(InstaFile.read(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel + "/arrestcount.txt"))) + 1);
                        InstaFile.rewrite(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel + "/arrestcount.txt"), b);
                        jonArrDayCounter ++;
                        sendMessage(channel, "LUL He got arrested " + b + "x LUL and just " + jonArrDayCounter + "x today 4Head");
                    }
                    break;
                case "!arrests":
                    String b = InstaFile.read(InstaFile.escaped("C:/gameData/RevloCrack/data/" + channel + "/arrestcount.txt"));
                    sendMessage(channel, "Mr Lewis got arrested " + b + "x and just " + jonCounterDay + "x today 4Head");
                    break;
            }
        }
    }

    public void onUserList(String channel, User[] users) {
        for(User user : users) {
            channels.get(channel).users.put(user.getNick().toLowerCase(), new TwitchUser(user.getNick(), channels.get(channel)));
        }
    }


    public void onJoin(String channel,
                       String sender,
                       String login,
                       String hostname) {
        channels.get(channel).joined(sender);
    }
}
